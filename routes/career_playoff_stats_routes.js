var express = require('express');
var router = express.Router();
var career_playoff_stats_dal = require('../models/career_playoff_stats_dal');
var player_dal = require('../models/player_dal');

// View All players
router.get('/all', function(req, res) {
    career_playoff_stats_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('career_playoff_stats/CareerPlayoffStatsViewAll', { 'result':result });
        }
    });
});

module.exports = router;