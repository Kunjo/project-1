var express = require('express');
var router = express.Router();
var team_dal = require('../models/team_dal');

// View All teams
router.get('/all', function(req, res) {
    team_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('teams/teamsViewAll', { 'result':result });
        }
    });
});

module.exports = router;