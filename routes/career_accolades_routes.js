var express = require('express');
var router = express.Router();
var career_accolades_dal = require('../models/career_accolades_dal');
var player_dal = require('../models/player_dal');

// View All players
router.get('/all', function(req, res) {
    career_accolades_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('career_accolades/CareerAccoladesViewAll', { 'result':result });
        }
    });
});

module.exports = router;
